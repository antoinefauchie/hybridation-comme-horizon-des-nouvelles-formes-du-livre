# L’hybridation comme horizon des nouvelles formes du livre
## Une critique du livre numérique basée sur le concept d’Alessandro Ludovico

<small>Antoine Fauchié – [www.quaternum.net](https://www.quaternum.net)<br>
Les matinées critiques de Figura – Jeudi 28 novembre 2019<br>
Université de Montréal</small>

===n===
Merci à Béatrice, à Eugénie et à Figura pour cette invitation !
Même si je suis moins stressé que pour la formulation de mon hypothèse de recherche (j’ai une douleur intercostale rien qu’en prononçant ces mots), j’avoue être assez impressionné par le fait de parler devant des étudiants en littératures de langue française.
Voilà c’est dit.

===l===

> l’avenir du modèle éditorial/imprimé traditionnel […] semble plus incertain que jamais.  
**Alessandro Ludovico**, _Post-Digital Print_

===d===

## Préambule

===n===
Il me faut préciser qu’il s’agit d’une présentation d’une recherche en cours.
Et qu’il s’agit aussi d’un moment de transition entre une recherche amateure et une recherche qui s’académise.
D’une certaine façon pour moi cette présentation est une des dernières occasion de bricoler, je reviendrai sur ce terme auquel je tiens.

Merci de m’arrêter si je ne suis pas clair, et de ne pas attendre la fin de cette courte présentation pour intervenir.

===d===

## Intention

Considérer le livre dans toute sa pluralité et sa richesse, _avec_ le numérique.

===n===
Considérant que notre espace est désormais numérique (il ne peut pas ne pas l’être), je ne cite pas de référence précise mais l’une d’elle est dans ce couloir et est mon directeur de thèse.

===d===

## Plan

1. Introduction : contexte, problème et hypothèse
2. Déconstruction du livre numérique
3. L’horizon de l’hybridation
4. Quelques illustrations de l’hybridation

===l===

## 1. Introduction

===n===
Tentative de structuration de ma recherche.

===d===

## 1. Introduction : contexte
Le livre numérique prend une place importante (représentation et usages) tout en étant actuellement et majoritairement limité à un objet homothétique.

===n===
Le livre numérique ne fait que reproduire le livre imprimé.
Comme nous allons le voir juste après la définition du livre numérique se base sur un état de fait, principalement économique.

===d===

## 1. Introduction : le double problème

Le numérique serait considéré comme une (nouvelle) rupture pour le livre.
Pourtant :

- le livre numérique n’est qu’une évolution dans le sillage de beaucoup d’autres
- le livre numérique n’en est qu’à ses balbutiements

===n===
Les autres évolutions sont nombreuses, et se suivent et se superposent : formats, méthodes d’impression, modes de diffusion et de distribution, etc.

Les balbutiements sont plus en terme d’usages que de potentialités techniques, c’est-à-dire que les moyens existent mais qu’ils ne sont pas mis en œuvre.

===d===

## 1. Introduction : hypothèse

L’hybridation est un moyen d’envisager l’intégration du livre dans un espace désormais numérique.

===n===


===l===

## 2. Déconstruction du livre numérique

===n===

===d===

## 2. Déconstruction du livre numérique : le livre numérique homothétique

Un livre numérique homothétique a les mêmes propriétés que son homologue imprimé : pagination, lecture linéaire organisée, navigation dans une structure, etc.

===n===
Il s’agit de l’immense majorité des livres numériques disponibles (commercialisés ou accessibles dans des dépôts ouverts).

La nouvelle dimension que porte le livre numérique est une forme de dématérialisation : à l’objet imprimé est remplacé par un support écran qui permet de consulter un ensemble de données.
J’insiste sur le terme de "remplacement", puisque le livre numérique est considéré comme indépendant.

===d===

## 2. Déconstruction du livre numérique : le livre numérique enrichi

Le livre numérique enrichi propose des propriétés inédites par rapport au livre imprimé : navigation hypertextuelle, intégration de contenus multimédias, modification du contenu, réinscriptibilité, etc.

===n===
Si le livre numérique enrichi semble plein de promesses, il reste une exception et pose des problèmes

===d===

## 2. Déconstruction du livre numérique : les limites de l’homothétie

L’homothétie née de la dissociation entre un objet imprimé et un objet numérique, plutôt que les comparer il faut envisager leur **complémentarité**.

===n===


===l===

## 3. L’horizon de l’hybridation

===n===
Lier les différentes formes du livre permettraient de dépasser les limites du livre numérique, assez pauvre en l’état (même s’il répond à des réels besoins dans sa forme homothétique).
Ainsi la version numérique d’un livre ne serait pas forcément pleine d’enrichissements (interactions, images animées, réinscriptibilité), mais tout simplement différente.
C’est ce que nous allons voir avec ce concept d’hybridation.

===d===

## 3. L’horizon de l’hybridation : de quoi parle-t-on ?

Pour résumer succinctement _Post-Digital Print_ d’Alessandro Ludovico :

- pas de "mort du papier"
- nombreuses expérimentations éditoriales (presse, livre) depuis 1950 (avant le "numérique")
- importance de l’archivage et du réseau
- hybridation

===n===
Note importante : ce livre a été écrit initialement en 2012, puis traduit en français en 2016. Il s'est passé des choses depuis. Par ailleurs je suis assez critique avec certaines idées avancées par Alessandro Ludovico, mais ce n'est pas l'objet de cette présentation.

Un des exemples les plus illustratif : l’auteur américain Cory Doctorow propose des formats numériques gratuits (PDF, EPUB ou audio) et imagine plusieurs formes de livres imprimés – du format poche imprimé à la demande via la plateforme Lulu.com à la version plus luxueuse fabriquée par des artisans du livre en nombre limité –, et des façons originales d’y accéder. Le livre numérique peut devenir un produit d’appel, gratuit, pour déclencher des ventes de livres imprimés.

===d===

## 3. L’horizon de l’hybridation : "l’impression postnumérique"

Combinaison de plusieurs éléments :

- souscription
- hybridation papier et numérique
- utilisation de l’informatique pour la production
- stratégies _cross media_

===n===
Ce qui apparaît dans l’ouvrage d’Alessandro Ludovico c’est que la plupart des initiatives stimulantes sont des entreprises individuelles d’artistes (écrivain, plasticien, etc.).

===d===

## 3. L’horizon de l’hybridation : changement de paradigme

Ce n’est donc pas la question du livre qui devrait nous préoccuper, mais plutôt celle de l’_écriture_.

===n===
Dans certaines situations l’édition (et peut-être l’écriture) devrait consister non plus dans le fait de figer un texte, mais d’étendre le contenu et d’anticiper des situations de lecture.

C’est un changement assez profond, puisque cela implique d’être capable de penser différentes formes pour un livre, mais aussi d’être en capacité de les produire.

===l===

## 4. Quelques illustrations de l’hybridation

===d===

## 4. Quelques illustrations de l’hybridation

3 exemples dans des champs divers :

- _Roman Mosaics in the J. Paul Getty Museum_, Alexis Belis, Getty Publications
- _Exigeons de meilleures bibliothèques_, R. David Lankes, Les Ateliers de [sens public]
- _Trésors et Trouvailles_, Joséphine Lanesem, Abrüpt

===n===


===l===

> Cette mutation, on peut le présumer, ne sera ni simple ni sans détours.  
**Alessandro Ludovico**, _Post-Digital Print_

Antoine Fauchié  
[antoine.fauchie@umontreal.ca](mailto:antoine.fauchie@umontreal.ca)  
[www.quaternum.net](https://www.quaternum.net)
