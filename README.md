# L'hybridation comme horizon des nouvelles formes du livre : une critique du livre numérique basée sur le concept d'Alessandro Ludovico

Cadre : matinée critique de Figura, un temps d'échange proposé par les étudiants de l'UQÀM et de l'Université de Montréal ([lien](http://figura.uqam.ca/actualites/les-matinees-critiques-du-comite-etudiant-figura-1)), jeudi 28 novembre 2019 de 10h à 12h au CRILQ (Université de Montréal, Montréal).

Objectif : tenter de démonter l'idée de rupture qui accompagne souvent l'avènement du numérique dans l'édition, comme cela a été le cas avec l'imprimerie. Je me baserai principalement sur l'ouvrage d'Alessandro Ludovico, Post-digital print: la mutation de l’édition depuis 1894, ainsi que sur plusieurs initiatives d'édition hybride dans le champ de la littérature et de l'édition universitaire.

## Résumé de l'intervention

- le contexte : qu'est-ce qu'un livre numérique ? Concept souvent réducteur, le livre dans sa version numérique restant laborieusement homothétique, hormis quelques expérimentations d'édition numérique enrichie ;
- le problème : le livre numérique imposerait une rupture, alors que dans les faits nous en sommes à une reproduction numérique (sous diverses formes) ;
- notre hypothèse : la rupture est plutôt évolution, aussi profonde soit-elle, à travers une _hybridation_. Définition de l'hybridation ;
- premier volet de la solution, sortir de la rupture : le numérique _ajoute_ une dimension au livre, qui n'est pas meilleure ou pire, simplement différente ;
- second volet de la solution, la perspective de l'hybridation : un livre imprimé peut être étendu à d'autres formes ou artéfacts comme des livres numériques, des diffusions partielles, d'autres formes imprimées, etc. ;
- conclusion : ce n'est donc pas la question du livre mais plutôt celle de l'écriture. Comment écrire en numérique ? Ouverture.

## Plan

1. introduction : contexte, problème et hypothèse
2. le livre numérique : déconstruction
3. l'hybridation : un horizon
4. quelques exemples pratiques autour de livres web (mais pas que)
